numpy

[all]
imagecodecs>=2024.12.30
matplotlib
defusedxml
lxml
zarr<3
fsspec

[codecs]
imagecodecs>=2024.12.30

[plot]
matplotlib

[test]
pytest
imagecodecs
czifile
cmapfile
oiffile
lfdfiles
psdtags
roifile
lxml
zarr<3
dask
xarray
fsspec
defusedxml
ndtiff

[xml]
defusedxml
lxml

[zarr]
zarr<3
fsspec
